# qrcode-save2zip

数据生成二维码，并本地保存为zip压缩文件

[传送门](https://songwang-cn.gitee.io/qrcode-save2zip/#/)

![输入图片说明](src/assets/result.png)


## 安装依赖

```sh
npm install file-saver 
```
```sh
npm install jszip 
```
```sh
npm install qrcodejs2-fix
```
base64编码 可选
```sh
npm install js-base64 
```


## 项目运行

```sh
npm install
```

```sh
npm run dev
```